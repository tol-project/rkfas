require(KFAS)

nileKFAS = function(x)
{
  H = exp(x[1])
  Q = exp(x[2])
  #, a1=0, P1=1e7
  SSModel(y=Nile, Z=1, H=H, T=1, R=1, Q=Q)
}

nileLogLik <- function(theta)
{
  mdl = SSModel(y = Nile, Z = 1, T = 1, R = 1, H = exp(theta[1]),
                Q = exp(theta[2]))
  kfs = KFS(mdl)              
  return(-kfs$logLik)
}

fit1 = fitSSM(c(100,2), modFun=nileKFAS)
mdl1 = nileKFAS(fit$opt$par)
kfs1 = KFS(mdl1)

fit2 = optim(par = c(100, 2), fn = nileLogLik)
mdl2 = nileKFAS(fit2$par)
kfs2 = KFS(mdl2)
